package main

import "testing"

func TestGenID(t *testing.T) {

	id := GenID()

	// Tout ID doit être inférieur
	// ou égal à 1000
	if id > 1000 {
		t.Errorf("ID invalide, plus grand que 1000 => %v", id)
	}
}